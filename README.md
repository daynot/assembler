# Assembler

El lenguaje ensamblador o assembler (en inglés: assembler language y la abreviación asm) es un lenguaje de programación que se usa en los microprocesadores. Implementa una representación simbólica de los códigos de máquina binarios y otras constantes necesarias para programar una arquitectura de procesador y constituye la representación más directa del código máquina específico para cada arquitectura legible por un programador. Cada arquitectura de procesador tiene su propio lenguaje ensamblador que usualmente es definida por el fabricante de hardware, y está basada en los mnemónicos que simbolizan los pasos de procesamiento (las instrucciones), los registros del procesador, las posiciones de memoria y otras características del lenguaje. Un lenguaje ensamblador es por lo tanto específico de cierta arquitectura de computador física (o virtual). Esto está en contraste con la mayoría de los lenguajes de programación de alto nivel, que idealmente son portables. (Wikipedia)

## Uso
El  uso  del lenguaje  ensamblador  le  permite  al  programador  indicarle  al  computadorexactamente  cómo  llevar  a  cabo una tarea específica usando la menor cantidad de instrucciones. Aún cuando el código generado por los compiladores con  opción  de  optimización  es  eficiente,  la  optimización  manual  puede  resultar  en  una mejora  sustancial  en  términos de   rendimiento   y   consumo   de   memoria.   El   lenguaje   ensamblador   es   usualmente   utilizado   en   las   siguientes circunstancias:Mejorar la eficiencia de una rutina específica que se ha transformado en un cuello de botella.Obtener acceso a funciones de bajo nivel del procesador para realizar tareas que no son soportadas por los lenguajes de alto nivel.Escribir manejadores de dispositivos para comunicarse directamente con hardware especial tales como tarjetas de red.Trabajar en ambientes con recursos limitados puede requerir el uso del lenguaje ensamblador pues el código ejecutable puede ser menor que el generado por el compilador.

## Principales comandos
MOV Transfiere datos entre celdas de memoria y registros.
```
mov ax, 10
```

INC Incrementa en una unidad al resgitro
```
inc ax
```

ADD Incrementa un valor a un resgitro
```
add ax, 10
```
